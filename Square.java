import java.lang.reflect.Constructor;

import org.junit.Test;

public class Square {
    private double size;
    Square(double size) {
        this.size = size;
    }

    public double getSize() {
        return this.size;
    }

    public double getArea() {
        return this.size * this.size;
    }

    public String toString() {
        return "Square, size: " + this.size;
    }
}
