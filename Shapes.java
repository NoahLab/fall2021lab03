class Shapes {
    public static void main(String[] args) {
        System.out.println("--- Circle ---");
        Circle circle = new Circle(5.0);
        System.out.println(circle);
        System.out.println("Circle area: " + circle.getArea());

        System.out.println("--- Square ---");
        Square mySquare = new Square(54.3);
        System.out.println(mySquare);
        System.out.println("Square area: " + mySquare.getArea());
    }
}
