import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;


public class SquareTests {
    @Test
    public void testConstructor() {
        Square square1 = new Square(5.2);
        Square square2 = new Square(52.0);
        Square square3 = new Square(502.0);
        Square square4 = new Square(53.4);
        Square square5 = new Square(1.8);
        Square square6 = new Square(10.0);

        assertEquals(5.2, square1.getSize());
        assertEquals(52.0, square2.getSize());
        assertEquals(502.0, square3.getSize());
        assertEquals(53.4, square4.getSize());
        assertEquals(1.8, square5.getSize());
        assertEquals(10.0, square6.getSize());
        
        assertEquals(5.2 * 5.2, square1.getArea());
        assertEquals(52.0 * 52.0, square2.getArea());
        assertEquals(502.0 * 502.0, square3.getArea());
        assertEquals(53.4 * 53.4, square4.getArea());
        assertEquals(1.8 * 1.8, square5.getArea());
        assertEquals(10.0 * 10.0, square6.getArea());
    }
}
