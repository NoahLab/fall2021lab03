import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class CircleTests {
    @Test
    public void testGetRadius() {
        Circle circle = new Circle(5.0);
        assertEquals(5.0, circle.getRadius());
    }

    @Test
    public void testGetArea() {
        Circle circle = new Circle(2.0);
        assertEquals(4.0 * Math.PI, circle.getArea());
    }

    @Test
    public void testToString() {
        Circle circle = new Circle(5.0);
        assertEquals("Circle with radius of 5.0", circle.toString());
    }
}
